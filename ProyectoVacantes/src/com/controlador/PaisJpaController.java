/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import com.controlador.exceptions.IllegalOrphanException;
import com.controlador.exceptions.NonexistentEntityException;
import com.controlador.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.modelo.Empleadoasalariado;
import java.util.ArrayList;
import java.util.List;
import com.modelo.Candidato;
import com.modelo.Encargado;
import com.modelo.Empleadohora;
import com.modelo.Empresa;
import com.modelo.Pais;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author hola
 */
public class PaisJpaController implements Serializable {

    public PaisJpaController(EntityManagerFactory emf) {
              this.emf = Persistence.createEntityManagerFactory("ProyectoVacantesPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Pais pais) throws PreexistingEntityException, Exception {
        if (pais.getEmpleadoasalariadoList() == null) {
            pais.setEmpleadoasalariadoList(new ArrayList<Empleadoasalariado>());
        }
        if (pais.getCandidatoList() == null) {
            pais.setCandidatoList(new ArrayList<Candidato>());
        }
        if (pais.getEncargadoList() == null) {
            pais.setEncargadoList(new ArrayList<Encargado>());
        }
        if (pais.getEmpleadohoraList() == null) {
            pais.setEmpleadohoraList(new ArrayList<Empleadohora>());
        }
        if (pais.getEmpresaList() == null) {
            pais.setEmpresaList(new ArrayList<Empresa>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Empleadoasalariado> attachedEmpleadoasalariadoList = new ArrayList<Empleadoasalariado>();
            for (Empleadoasalariado empleadoasalariadoListEmpleadoasalariadoToAttach : pais.getEmpleadoasalariadoList()) {
                empleadoasalariadoListEmpleadoasalariadoToAttach = em.getReference(empleadoasalariadoListEmpleadoasalariadoToAttach.getClass(), empleadoasalariadoListEmpleadoasalariadoToAttach.getId());
                attachedEmpleadoasalariadoList.add(empleadoasalariadoListEmpleadoasalariadoToAttach);
            }
            pais.setEmpleadoasalariadoList(attachedEmpleadoasalariadoList);
            List<Candidato> attachedCandidatoList = new ArrayList<Candidato>();
            for (Candidato candidatoListCandidatoToAttach : pais.getCandidatoList()) {
                candidatoListCandidatoToAttach = em.getReference(candidatoListCandidatoToAttach.getClass(), candidatoListCandidatoToAttach.getId());
                attachedCandidatoList.add(candidatoListCandidatoToAttach);
            }
            pais.setCandidatoList(attachedCandidatoList);
            List<Encargado> attachedEncargadoList = new ArrayList<Encargado>();
            for (Encargado encargadoListEncargadoToAttach : pais.getEncargadoList()) {
                encargadoListEncargadoToAttach = em.getReference(encargadoListEncargadoToAttach.getClass(), encargadoListEncargadoToAttach.getId());
                attachedEncargadoList.add(encargadoListEncargadoToAttach);
            }
            pais.setEncargadoList(attachedEncargadoList);
            List<Empleadohora> attachedEmpleadohoraList = new ArrayList<Empleadohora>();
            for (Empleadohora empleadohoraListEmpleadohoraToAttach : pais.getEmpleadohoraList()) {
                empleadohoraListEmpleadohoraToAttach = em.getReference(empleadohoraListEmpleadohoraToAttach.getClass(), empleadohoraListEmpleadohoraToAttach.getId());
                attachedEmpleadohoraList.add(empleadohoraListEmpleadohoraToAttach);
            }
            pais.setEmpleadohoraList(attachedEmpleadohoraList);
            List<Empresa> attachedEmpresaList = new ArrayList<Empresa>();
            for (Empresa empresaListEmpresaToAttach : pais.getEmpresaList()) {
                empresaListEmpresaToAttach = em.getReference(empresaListEmpresaToAttach.getClass(), empresaListEmpresaToAttach.getId());
                attachedEmpresaList.add(empresaListEmpresaToAttach);
            }
            pais.setEmpresaList(attachedEmpresaList);
            em.persist(pais);
            for (Empleadoasalariado empleadoasalariadoListEmpleadoasalariado : pais.getEmpleadoasalariadoList()) {
                Pais oldNacionalidadOfEmpleadoasalariadoListEmpleadoasalariado = empleadoasalariadoListEmpleadoasalariado.getNacionalidad();
                empleadoasalariadoListEmpleadoasalariado.setNacionalidad(pais);
                empleadoasalariadoListEmpleadoasalariado = em.merge(empleadoasalariadoListEmpleadoasalariado);
                if (oldNacionalidadOfEmpleadoasalariadoListEmpleadoasalariado != null) {
                    oldNacionalidadOfEmpleadoasalariadoListEmpleadoasalariado.getEmpleadoasalariadoList().remove(empleadoasalariadoListEmpleadoasalariado);
                    oldNacionalidadOfEmpleadoasalariadoListEmpleadoasalariado = em.merge(oldNacionalidadOfEmpleadoasalariadoListEmpleadoasalariado);
                }
            }
            for (Candidato candidatoListCandidato : pais.getCandidatoList()) {
                Pais oldNacionalidadOfCandidatoListCandidato = candidatoListCandidato.getNacionalidad();
                candidatoListCandidato.setNacionalidad(pais);
                candidatoListCandidato = em.merge(candidatoListCandidato);
                if (oldNacionalidadOfCandidatoListCandidato != null) {
                    oldNacionalidadOfCandidatoListCandidato.getCandidatoList().remove(candidatoListCandidato);
                    oldNacionalidadOfCandidatoListCandidato = em.merge(oldNacionalidadOfCandidatoListCandidato);
                }
            }
            for (Encargado encargadoListEncargado : pais.getEncargadoList()) {
                Pais oldNacionalidadOfEncargadoListEncargado = encargadoListEncargado.getNacionalidad();
                encargadoListEncargado.setNacionalidad(pais);
                encargadoListEncargado = em.merge(encargadoListEncargado);
                if (oldNacionalidadOfEncargadoListEncargado != null) {
                    oldNacionalidadOfEncargadoListEncargado.getEncargadoList().remove(encargadoListEncargado);
                    oldNacionalidadOfEncargadoListEncargado = em.merge(oldNacionalidadOfEncargadoListEncargado);
                }
            }
            for (Empleadohora empleadohoraListEmpleadohora : pais.getEmpleadohoraList()) {
                Pais oldNacionalidadOfEmpleadohoraListEmpleadohora = empleadohoraListEmpleadohora.getNacionalidad();
                empleadohoraListEmpleadohora.setNacionalidad(pais);
                empleadohoraListEmpleadohora = em.merge(empleadohoraListEmpleadohora);
                if (oldNacionalidadOfEmpleadohoraListEmpleadohora != null) {
                    oldNacionalidadOfEmpleadohoraListEmpleadohora.getEmpleadohoraList().remove(empleadohoraListEmpleadohora);
                    oldNacionalidadOfEmpleadohoraListEmpleadohora = em.merge(oldNacionalidadOfEmpleadohoraListEmpleadohora);
                }
            }
            for (Empresa empresaListEmpresa : pais.getEmpresaList()) {
                Pais oldNacionalidadOfEmpresaListEmpresa = empresaListEmpresa.getNacionalidad();
                empresaListEmpresa.setNacionalidad(pais);
                empresaListEmpresa = em.merge(empresaListEmpresa);
                if (oldNacionalidadOfEmpresaListEmpresa != null) {
                    oldNacionalidadOfEmpresaListEmpresa.getEmpresaList().remove(empresaListEmpresa);
                    oldNacionalidadOfEmpresaListEmpresa = em.merge(oldNacionalidadOfEmpresaListEmpresa);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPais(pais.getId()) != null) {
                throw new PreexistingEntityException("Pais " + pais + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Pais pais) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pais persistentPais = em.find(Pais.class, pais.getId());
            List<Empleadoasalariado> empleadoasalariadoListOld = persistentPais.getEmpleadoasalariadoList();
            List<Empleadoasalariado> empleadoasalariadoListNew = pais.getEmpleadoasalariadoList();
            List<Candidato> candidatoListOld = persistentPais.getCandidatoList();
            List<Candidato> candidatoListNew = pais.getCandidatoList();
            List<Encargado> encargadoListOld = persistentPais.getEncargadoList();
            List<Encargado> encargadoListNew = pais.getEncargadoList();
            List<Empleadohora> empleadohoraListOld = persistentPais.getEmpleadohoraList();
            List<Empleadohora> empleadohoraListNew = pais.getEmpleadohoraList();
            List<Empresa> empresaListOld = persistentPais.getEmpresaList();
            List<Empresa> empresaListNew = pais.getEmpresaList();
            List<String> illegalOrphanMessages = null;
            for (Empleadoasalariado empleadoasalariadoListOldEmpleadoasalariado : empleadoasalariadoListOld) {
                if (!empleadoasalariadoListNew.contains(empleadoasalariadoListOldEmpleadoasalariado)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Empleadoasalariado " + empleadoasalariadoListOldEmpleadoasalariado + " since its nacionalidad field is not nullable.");
                }
            }
            for (Candidato candidatoListOldCandidato : candidatoListOld) {
                if (!candidatoListNew.contains(candidatoListOldCandidato)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Candidato " + candidatoListOldCandidato + " since its nacionalidad field is not nullable.");
                }
            }
            for (Encargado encargadoListOldEncargado : encargadoListOld) {
                if (!encargadoListNew.contains(encargadoListOldEncargado)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Encargado " + encargadoListOldEncargado + " since its nacionalidad field is not nullable.");
                }
            }
            for (Empleadohora empleadohoraListOldEmpleadohora : empleadohoraListOld) {
                if (!empleadohoraListNew.contains(empleadohoraListOldEmpleadohora)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Empleadohora " + empleadohoraListOldEmpleadohora + " since its nacionalidad field is not nullable.");
                }
            }
            for (Empresa empresaListOldEmpresa : empresaListOld) {
                if (!empresaListNew.contains(empresaListOldEmpresa)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Empresa " + empresaListOldEmpresa + " since its nacionalidad field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Empleadoasalariado> attachedEmpleadoasalariadoListNew = new ArrayList<Empleadoasalariado>();
            for (Empleadoasalariado empleadoasalariadoListNewEmpleadoasalariadoToAttach : empleadoasalariadoListNew) {
                empleadoasalariadoListNewEmpleadoasalariadoToAttach = em.getReference(empleadoasalariadoListNewEmpleadoasalariadoToAttach.getClass(), empleadoasalariadoListNewEmpleadoasalariadoToAttach.getId());
                attachedEmpleadoasalariadoListNew.add(empleadoasalariadoListNewEmpleadoasalariadoToAttach);
            }
            empleadoasalariadoListNew = attachedEmpleadoasalariadoListNew;
            pais.setEmpleadoasalariadoList(empleadoasalariadoListNew);
            List<Candidato> attachedCandidatoListNew = new ArrayList<Candidato>();
            for (Candidato candidatoListNewCandidatoToAttach : candidatoListNew) {
                candidatoListNewCandidatoToAttach = em.getReference(candidatoListNewCandidatoToAttach.getClass(), candidatoListNewCandidatoToAttach.getId());
                attachedCandidatoListNew.add(candidatoListNewCandidatoToAttach);
            }
            candidatoListNew = attachedCandidatoListNew;
            pais.setCandidatoList(candidatoListNew);
            List<Encargado> attachedEncargadoListNew = new ArrayList<Encargado>();
            for (Encargado encargadoListNewEncargadoToAttach : encargadoListNew) {
                encargadoListNewEncargadoToAttach = em.getReference(encargadoListNewEncargadoToAttach.getClass(), encargadoListNewEncargadoToAttach.getId());
                attachedEncargadoListNew.add(encargadoListNewEncargadoToAttach);
            }
            encargadoListNew = attachedEncargadoListNew;
            pais.setEncargadoList(encargadoListNew);
            List<Empleadohora> attachedEmpleadohoraListNew = new ArrayList<Empleadohora>();
            for (Empleadohora empleadohoraListNewEmpleadohoraToAttach : empleadohoraListNew) {
                empleadohoraListNewEmpleadohoraToAttach = em.getReference(empleadohoraListNewEmpleadohoraToAttach.getClass(), empleadohoraListNewEmpleadohoraToAttach.getId());
                attachedEmpleadohoraListNew.add(empleadohoraListNewEmpleadohoraToAttach);
            }
            empleadohoraListNew = attachedEmpleadohoraListNew;
            pais.setEmpleadohoraList(empleadohoraListNew);
            List<Empresa> attachedEmpresaListNew = new ArrayList<Empresa>();
            for (Empresa empresaListNewEmpresaToAttach : empresaListNew) {
                empresaListNewEmpresaToAttach = em.getReference(empresaListNewEmpresaToAttach.getClass(), empresaListNewEmpresaToAttach.getId());
                attachedEmpresaListNew.add(empresaListNewEmpresaToAttach);
            }
            empresaListNew = attachedEmpresaListNew;
            pais.setEmpresaList(empresaListNew);
            pais = em.merge(pais);
            for (Empleadoasalariado empleadoasalariadoListNewEmpleadoasalariado : empleadoasalariadoListNew) {
                if (!empleadoasalariadoListOld.contains(empleadoasalariadoListNewEmpleadoasalariado)) {
                    Pais oldNacionalidadOfEmpleadoasalariadoListNewEmpleadoasalariado = empleadoasalariadoListNewEmpleadoasalariado.getNacionalidad();
                    empleadoasalariadoListNewEmpleadoasalariado.setNacionalidad(pais);
                    empleadoasalariadoListNewEmpleadoasalariado = em.merge(empleadoasalariadoListNewEmpleadoasalariado);
                    if (oldNacionalidadOfEmpleadoasalariadoListNewEmpleadoasalariado != null && !oldNacionalidadOfEmpleadoasalariadoListNewEmpleadoasalariado.equals(pais)) {
                        oldNacionalidadOfEmpleadoasalariadoListNewEmpleadoasalariado.getEmpleadoasalariadoList().remove(empleadoasalariadoListNewEmpleadoasalariado);
                        oldNacionalidadOfEmpleadoasalariadoListNewEmpleadoasalariado = em.merge(oldNacionalidadOfEmpleadoasalariadoListNewEmpleadoasalariado);
                    }
                }
            }
            for (Candidato candidatoListNewCandidato : candidatoListNew) {
                if (!candidatoListOld.contains(candidatoListNewCandidato)) {
                    Pais oldNacionalidadOfCandidatoListNewCandidato = candidatoListNewCandidato.getNacionalidad();
                    candidatoListNewCandidato.setNacionalidad(pais);
                    candidatoListNewCandidato = em.merge(candidatoListNewCandidato);
                    if (oldNacionalidadOfCandidatoListNewCandidato != null && !oldNacionalidadOfCandidatoListNewCandidato.equals(pais)) {
                        oldNacionalidadOfCandidatoListNewCandidato.getCandidatoList().remove(candidatoListNewCandidato);
                        oldNacionalidadOfCandidatoListNewCandidato = em.merge(oldNacionalidadOfCandidatoListNewCandidato);
                    }
                }
            }
            for (Encargado encargadoListNewEncargado : encargadoListNew) {
                if (!encargadoListOld.contains(encargadoListNewEncargado)) {
                    Pais oldNacionalidadOfEncargadoListNewEncargado = encargadoListNewEncargado.getNacionalidad();
                    encargadoListNewEncargado.setNacionalidad(pais);
                    encargadoListNewEncargado = em.merge(encargadoListNewEncargado);
                    if (oldNacionalidadOfEncargadoListNewEncargado != null && !oldNacionalidadOfEncargadoListNewEncargado.equals(pais)) {
                        oldNacionalidadOfEncargadoListNewEncargado.getEncargadoList().remove(encargadoListNewEncargado);
                        oldNacionalidadOfEncargadoListNewEncargado = em.merge(oldNacionalidadOfEncargadoListNewEncargado);
                    }
                }
            }
            for (Empleadohora empleadohoraListNewEmpleadohora : empleadohoraListNew) {
                if (!empleadohoraListOld.contains(empleadohoraListNewEmpleadohora)) {
                    Pais oldNacionalidadOfEmpleadohoraListNewEmpleadohora = empleadohoraListNewEmpleadohora.getNacionalidad();
                    empleadohoraListNewEmpleadohora.setNacionalidad(pais);
                    empleadohoraListNewEmpleadohora = em.merge(empleadohoraListNewEmpleadohora);
                    if (oldNacionalidadOfEmpleadohoraListNewEmpleadohora != null && !oldNacionalidadOfEmpleadohoraListNewEmpleadohora.equals(pais)) {
                        oldNacionalidadOfEmpleadohoraListNewEmpleadohora.getEmpleadohoraList().remove(empleadohoraListNewEmpleadohora);
                        oldNacionalidadOfEmpleadohoraListNewEmpleadohora = em.merge(oldNacionalidadOfEmpleadohoraListNewEmpleadohora);
                    }
                }
            }
            for (Empresa empresaListNewEmpresa : empresaListNew) {
                if (!empresaListOld.contains(empresaListNewEmpresa)) {
                    Pais oldNacionalidadOfEmpresaListNewEmpresa = empresaListNewEmpresa.getNacionalidad();
                    empresaListNewEmpresa.setNacionalidad(pais);
                    empresaListNewEmpresa = em.merge(empresaListNewEmpresa);
                    if (oldNacionalidadOfEmpresaListNewEmpresa != null && !oldNacionalidadOfEmpresaListNewEmpresa.equals(pais)) {
                        oldNacionalidadOfEmpresaListNewEmpresa.getEmpresaList().remove(empresaListNewEmpresa);
                        oldNacionalidadOfEmpresaListNewEmpresa = em.merge(oldNacionalidadOfEmpresaListNewEmpresa);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = pais.getId();
                if (findPais(id) == null) {
                    throw new NonexistentEntityException("The pais with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pais pais;
            try {
                pais = em.getReference(Pais.class, id);
                pais.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pais with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Empleadoasalariado> empleadoasalariadoListOrphanCheck = pais.getEmpleadoasalariadoList();
            for (Empleadoasalariado empleadoasalariadoListOrphanCheckEmpleadoasalariado : empleadoasalariadoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Pais (" + pais + ") cannot be destroyed since the Empleadoasalariado " + empleadoasalariadoListOrphanCheckEmpleadoasalariado + " in its empleadoasalariadoList field has a non-nullable nacionalidad field.");
            }
            List<Candidato> candidatoListOrphanCheck = pais.getCandidatoList();
            for (Candidato candidatoListOrphanCheckCandidato : candidatoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Pais (" + pais + ") cannot be destroyed since the Candidato " + candidatoListOrphanCheckCandidato + " in its candidatoList field has a non-nullable nacionalidad field.");
            }
            List<Encargado> encargadoListOrphanCheck = pais.getEncargadoList();
            for (Encargado encargadoListOrphanCheckEncargado : encargadoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Pais (" + pais + ") cannot be destroyed since the Encargado " + encargadoListOrphanCheckEncargado + " in its encargadoList field has a non-nullable nacionalidad field.");
            }
            List<Empleadohora> empleadohoraListOrphanCheck = pais.getEmpleadohoraList();
            for (Empleadohora empleadohoraListOrphanCheckEmpleadohora : empleadohoraListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Pais (" + pais + ") cannot be destroyed since the Empleadohora " + empleadohoraListOrphanCheckEmpleadohora + " in its empleadohoraList field has a non-nullable nacionalidad field.");
            }
            List<Empresa> empresaListOrphanCheck = pais.getEmpresaList();
            for (Empresa empresaListOrphanCheckEmpresa : empresaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Pais (" + pais + ") cannot be destroyed since the Empresa " + empresaListOrphanCheckEmpresa + " in its empresaList field has a non-nullable nacionalidad field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(pais);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Pais> findPaisEntities() {
        return findPaisEntities(true, -1, -1);
    }

    public List<Pais> findPaisEntities(int maxResults, int firstResult) {
        return findPaisEntities(false, maxResults, firstResult);
    }

    private List<Pais> findPaisEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Pais.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Pais findPais(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Pais.class, id);
        } finally {
            em.close();
        }
    }

    public int getPaisCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Pais> rt = cq.from(Pais.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
