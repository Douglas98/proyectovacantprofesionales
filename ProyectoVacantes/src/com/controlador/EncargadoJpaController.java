/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import com.controlador.exceptions.IllegalOrphanException;
import com.controlador.exceptions.NonexistentEntityException;
import com.controlador.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.modelo.Cargo;
import com.modelo.Pais;
import com.modelo.Profesion;
import com.modelo.Empresa;
import com.modelo.Encargado;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author hola
 */
public class EncargadoJpaController implements Serializable {

    public EncargadoJpaController(EntityManagerFactory emf) {
              this.emf = Persistence.createEntityManagerFactory("ProyectoVacantesPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Encargado encargado) throws PreexistingEntityException, Exception {
        if (encargado.getEmpresaList() == null) {
            encargado.setEmpresaList(new ArrayList<Empresa>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cargo cargoDesepennado = encargado.getCargoDesepennado();
            if (cargoDesepennado != null) {
                cargoDesepennado = em.getReference(cargoDesepennado.getClass(), cargoDesepennado.getId());
                encargado.setCargoDesepennado(cargoDesepennado);
            }
            Pais nacionalidad = encargado.getNacionalidad();
            if (nacionalidad != null) {
                nacionalidad = em.getReference(nacionalidad.getClass(), nacionalidad.getId());
                encargado.setNacionalidad(nacionalidad);
            }
            Profesion profesionEjercida = encargado.getProfesionEjercida();
            if (profesionEjercida != null) {
                profesionEjercida = em.getReference(profesionEjercida.getClass(), profesionEjercida.getId());
                encargado.setProfesionEjercida(profesionEjercida);
            }
            List<Empresa> attachedEmpresaList = new ArrayList<Empresa>();
            for (Empresa empresaListEmpresaToAttach : encargado.getEmpresaList()) {
                empresaListEmpresaToAttach = em.getReference(empresaListEmpresaToAttach.getClass(), empresaListEmpresaToAttach.getId());
                attachedEmpresaList.add(empresaListEmpresaToAttach);
            }
            encargado.setEmpresaList(attachedEmpresaList);
            em.persist(encargado);
            if (cargoDesepennado != null) {
                cargoDesepennado.getEncargadoList().add(encargado);
                cargoDesepennado = em.merge(cargoDesepennado);
            }
            if (nacionalidad != null) {
                nacionalidad.getEncargadoList().add(encargado);
                nacionalidad = em.merge(nacionalidad);
            }
            if (profesionEjercida != null) {
                profesionEjercida.getEncargadoList().add(encargado);
                profesionEjercida = em.merge(profesionEjercida);
            }
            for (Empresa empresaListEmpresa : encargado.getEmpresaList()) {
                Encargado oldEncargadoOfEmpresaListEmpresa = empresaListEmpresa.getEncargado();
                empresaListEmpresa.setEncargado(encargado);
                empresaListEmpresa = em.merge(empresaListEmpresa);
                if (oldEncargadoOfEmpresaListEmpresa != null) {
                    oldEncargadoOfEmpresaListEmpresa.getEmpresaList().remove(empresaListEmpresa);
                    oldEncargadoOfEmpresaListEmpresa = em.merge(oldEncargadoOfEmpresaListEmpresa);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findEncargado(encargado.getId()) != null) {
                throw new PreexistingEntityException("Encargado " + encargado + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Encargado encargado) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Encargado persistentEncargado = em.find(Encargado.class, encargado.getId());
            Cargo cargoDesepennadoOld = persistentEncargado.getCargoDesepennado();
            Cargo cargoDesepennadoNew = encargado.getCargoDesepennado();
            Pais nacionalidadOld = persistentEncargado.getNacionalidad();
            Pais nacionalidadNew = encargado.getNacionalidad();
            Profesion profesionEjercidaOld = persistentEncargado.getProfesionEjercida();
            Profesion profesionEjercidaNew = encargado.getProfesionEjercida();
            List<Empresa> empresaListOld = persistentEncargado.getEmpresaList();
            List<Empresa> empresaListNew = encargado.getEmpresaList();
            List<String> illegalOrphanMessages = null;
            for (Empresa empresaListOldEmpresa : empresaListOld) {
                if (!empresaListNew.contains(empresaListOldEmpresa)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Empresa " + empresaListOldEmpresa + " since its encargado field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (cargoDesepennadoNew != null) {
                cargoDesepennadoNew = em.getReference(cargoDesepennadoNew.getClass(), cargoDesepennadoNew.getId());
                encargado.setCargoDesepennado(cargoDesepennadoNew);
            }
            if (nacionalidadNew != null) {
                nacionalidadNew = em.getReference(nacionalidadNew.getClass(), nacionalidadNew.getId());
                encargado.setNacionalidad(nacionalidadNew);
            }
            if (profesionEjercidaNew != null) {
                profesionEjercidaNew = em.getReference(profesionEjercidaNew.getClass(), profesionEjercidaNew.getId());
                encargado.setProfesionEjercida(profesionEjercidaNew);
            }
            List<Empresa> attachedEmpresaListNew = new ArrayList<Empresa>();
            for (Empresa empresaListNewEmpresaToAttach : empresaListNew) {
                empresaListNewEmpresaToAttach = em.getReference(empresaListNewEmpresaToAttach.getClass(), empresaListNewEmpresaToAttach.getId());
                attachedEmpresaListNew.add(empresaListNewEmpresaToAttach);
            }
            empresaListNew = attachedEmpresaListNew;
            encargado.setEmpresaList(empresaListNew);
            encargado = em.merge(encargado);
            if (cargoDesepennadoOld != null && !cargoDesepennadoOld.equals(cargoDesepennadoNew)) {
                cargoDesepennadoOld.getEncargadoList().remove(encargado);
                cargoDesepennadoOld = em.merge(cargoDesepennadoOld);
            }
            if (cargoDesepennadoNew != null && !cargoDesepennadoNew.equals(cargoDesepennadoOld)) {
                cargoDesepennadoNew.getEncargadoList().add(encargado);
                cargoDesepennadoNew = em.merge(cargoDesepennadoNew);
            }
            if (nacionalidadOld != null && !nacionalidadOld.equals(nacionalidadNew)) {
                nacionalidadOld.getEncargadoList().remove(encargado);
                nacionalidadOld = em.merge(nacionalidadOld);
            }
            if (nacionalidadNew != null && !nacionalidadNew.equals(nacionalidadOld)) {
                nacionalidadNew.getEncargadoList().add(encargado);
                nacionalidadNew = em.merge(nacionalidadNew);
            }
            if (profesionEjercidaOld != null && !profesionEjercidaOld.equals(profesionEjercidaNew)) {
                profesionEjercidaOld.getEncargadoList().remove(encargado);
                profesionEjercidaOld = em.merge(profesionEjercidaOld);
            }
            if (profesionEjercidaNew != null && !profesionEjercidaNew.equals(profesionEjercidaOld)) {
                profesionEjercidaNew.getEncargadoList().add(encargado);
                profesionEjercidaNew = em.merge(profesionEjercidaNew);
            }
            for (Empresa empresaListNewEmpresa : empresaListNew) {
                if (!empresaListOld.contains(empresaListNewEmpresa)) {
                    Encargado oldEncargadoOfEmpresaListNewEmpresa = empresaListNewEmpresa.getEncargado();
                    empresaListNewEmpresa.setEncargado(encargado);
                    empresaListNewEmpresa = em.merge(empresaListNewEmpresa);
                    if (oldEncargadoOfEmpresaListNewEmpresa != null && !oldEncargadoOfEmpresaListNewEmpresa.equals(encargado)) {
                        oldEncargadoOfEmpresaListNewEmpresa.getEmpresaList().remove(empresaListNewEmpresa);
                        oldEncargadoOfEmpresaListNewEmpresa = em.merge(oldEncargadoOfEmpresaListNewEmpresa);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = encargado.getId();
                if (findEncargado(id) == null) {
                    throw new NonexistentEntityException("The encargado with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Encargado encargado;
            try {
                encargado = em.getReference(Encargado.class, id);
                encargado.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The encargado with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Empresa> empresaListOrphanCheck = encargado.getEmpresaList();
            for (Empresa empresaListOrphanCheckEmpresa : empresaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Encargado (" + encargado + ") cannot be destroyed since the Empresa " + empresaListOrphanCheckEmpresa + " in its empresaList field has a non-nullable encargado field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Cargo cargoDesepennado = encargado.getCargoDesepennado();
            if (cargoDesepennado != null) {
                cargoDesepennado.getEncargadoList().remove(encargado);
                cargoDesepennado = em.merge(cargoDesepennado);
            }
            Pais nacionalidad = encargado.getNacionalidad();
            if (nacionalidad != null) {
                nacionalidad.getEncargadoList().remove(encargado);
                nacionalidad = em.merge(nacionalidad);
            }
            Profesion profesionEjercida = encargado.getProfesionEjercida();
            if (profesionEjercida != null) {
                profesionEjercida.getEncargadoList().remove(encargado);
                profesionEjercida = em.merge(profesionEjercida);
            }
            em.remove(encargado);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Encargado> findEncargadoEntities() {
        return findEncargadoEntities(true, -1, -1);
    }

    public List<Encargado> findEncargadoEntities(int maxResults, int firstResult) {
        return findEncargadoEntities(false, maxResults, firstResult);
    }

    private List<Encargado> findEncargadoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Encargado.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Encargado findEncargado(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Encargado.class, id);
        } finally {
            em.close();
        }
    }

    public int getEncargadoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Encargado> rt = cq.from(Encargado.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
