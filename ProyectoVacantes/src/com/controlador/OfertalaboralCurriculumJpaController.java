/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import com.controlador.exceptions.NonexistentEntityException;
import com.controlador.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.modelo.Curriculum;
import com.modelo.Ofertalaboral;
import com.modelo.OfertalaboralCurriculum;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author hola
 */
public class OfertalaboralCurriculumJpaController implements Serializable {

    public OfertalaboralCurriculumJpaController(EntityManagerFactory emf) {
              this.emf = Persistence.createEntityManagerFactory("ProyectoVacantesPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(OfertalaboralCurriculum ofertalaboralCurriculum) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Curriculum curriculum = ofertalaboralCurriculum.getCurriculum();
            if (curriculum != null) {
                curriculum = em.getReference(curriculum.getClass(), curriculum.getId());
                ofertalaboralCurriculum.setCurriculum(curriculum);
            }
            Ofertalaboral ofertaLaboral = ofertalaboralCurriculum.getOfertaLaboral();
            if (ofertaLaboral != null) {
                ofertaLaboral = em.getReference(ofertaLaboral.getClass(), ofertaLaboral.getId());
                ofertalaboralCurriculum.setOfertaLaboral(ofertaLaboral);
            }
            em.persist(ofertalaboralCurriculum);
            if (curriculum != null) {
                curriculum.getOfertalaboralCurriculumList().add(ofertalaboralCurriculum);
                curriculum = em.merge(curriculum);
            }
            if (ofertaLaboral != null) {
                ofertaLaboral.getOfertalaboralCurriculumList().add(ofertalaboralCurriculum);
                ofertaLaboral = em.merge(ofertaLaboral);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findOfertalaboralCurriculum(ofertalaboralCurriculum.getId()) != null) {
                throw new PreexistingEntityException("OfertalaboralCurriculum " + ofertalaboralCurriculum + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(OfertalaboralCurriculum ofertalaboralCurriculum) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            OfertalaboralCurriculum persistentOfertalaboralCurriculum = em.find(OfertalaboralCurriculum.class, ofertalaboralCurriculum.getId());
            Curriculum curriculumOld = persistentOfertalaboralCurriculum.getCurriculum();
            Curriculum curriculumNew = ofertalaboralCurriculum.getCurriculum();
            Ofertalaboral ofertaLaboralOld = persistentOfertalaboralCurriculum.getOfertaLaboral();
            Ofertalaboral ofertaLaboralNew = ofertalaboralCurriculum.getOfertaLaboral();
            if (curriculumNew != null) {
                curriculumNew = em.getReference(curriculumNew.getClass(), curriculumNew.getId());
                ofertalaboralCurriculum.setCurriculum(curriculumNew);
            }
            if (ofertaLaboralNew != null) {
                ofertaLaboralNew = em.getReference(ofertaLaboralNew.getClass(), ofertaLaboralNew.getId());
                ofertalaboralCurriculum.setOfertaLaboral(ofertaLaboralNew);
            }
            ofertalaboralCurriculum = em.merge(ofertalaboralCurriculum);
            if (curriculumOld != null && !curriculumOld.equals(curriculumNew)) {
                curriculumOld.getOfertalaboralCurriculumList().remove(ofertalaboralCurriculum);
                curriculumOld = em.merge(curriculumOld);
            }
            if (curriculumNew != null && !curriculumNew.equals(curriculumOld)) {
                curriculumNew.getOfertalaboralCurriculumList().add(ofertalaboralCurriculum);
                curriculumNew = em.merge(curriculumNew);
            }
            if (ofertaLaboralOld != null && !ofertaLaboralOld.equals(ofertaLaboralNew)) {
                ofertaLaboralOld.getOfertalaboralCurriculumList().remove(ofertalaboralCurriculum);
                ofertaLaboralOld = em.merge(ofertaLaboralOld);
            }
            if (ofertaLaboralNew != null && !ofertaLaboralNew.equals(ofertaLaboralOld)) {
                ofertaLaboralNew.getOfertalaboralCurriculumList().add(ofertalaboralCurriculum);
                ofertaLaboralNew = em.merge(ofertaLaboralNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ofertalaboralCurriculum.getId();
                if (findOfertalaboralCurriculum(id) == null) {
                    throw new NonexistentEntityException("The ofertalaboralCurriculum with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            OfertalaboralCurriculum ofertalaboralCurriculum;
            try {
                ofertalaboralCurriculum = em.getReference(OfertalaboralCurriculum.class, id);
                ofertalaboralCurriculum.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ofertalaboralCurriculum with id " + id + " no longer exists.", enfe);
            }
            Curriculum curriculum = ofertalaboralCurriculum.getCurriculum();
            if (curriculum != null) {
                curriculum.getOfertalaboralCurriculumList().remove(ofertalaboralCurriculum);
                curriculum = em.merge(curriculum);
            }
            Ofertalaboral ofertaLaboral = ofertalaboralCurriculum.getOfertaLaboral();
            if (ofertaLaboral != null) {
                ofertaLaboral.getOfertalaboralCurriculumList().remove(ofertalaboralCurriculum);
                ofertaLaboral = em.merge(ofertaLaboral);
            }
            em.remove(ofertalaboralCurriculum);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<OfertalaboralCurriculum> findOfertalaboralCurriculumEntities() {
        return findOfertalaboralCurriculumEntities(true, -1, -1);
    }

    public List<OfertalaboralCurriculum> findOfertalaboralCurriculumEntities(int maxResults, int firstResult) {
        return findOfertalaboralCurriculumEntities(false, maxResults, firstResult);
    }

    private List<OfertalaboralCurriculum> findOfertalaboralCurriculumEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(OfertalaboralCurriculum.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public OfertalaboralCurriculum findOfertalaboralCurriculum(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(OfertalaboralCurriculum.class, id);
        } finally {
            em.close();
        }
    }

    public int getOfertalaboralCurriculumCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<OfertalaboralCurriculum> rt = cq.from(OfertalaboralCurriculum.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
