/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import com.controlador.exceptions.NonexistentEntityException;
import com.controlador.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.modelo.Cargo;
import com.modelo.Empleadohora;
import com.modelo.Municipio;
import com.modelo.Pais;
import com.modelo.Profesion;
import com.modelo.Usuario;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author hola
 */
public class EmpleadohoraJpaController implements Serializable {

    public EmpleadohoraJpaController(EntityManagerFactory emf) {
              this.emf = Persistence.createEntityManagerFactory("ProyectoVacantesPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Empleadohora empleadohora) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cargo cargoDesempennado = empleadohora.getCargoDesempennado();
            if (cargoDesempennado != null) {
                cargoDesempennado = em.getReference(cargoDesempennado.getClass(), cargoDesempennado.getId());
                empleadohora.setCargoDesempennado(cargoDesempennado);
            }
            Municipio municipioActual = empleadohora.getMunicipioActual();
            if (municipioActual != null) {
                municipioActual = em.getReference(municipioActual.getClass(), municipioActual.getId());
                empleadohora.setMunicipioActual(municipioActual);
            }
            Pais nacionalidad = empleadohora.getNacionalidad();
            if (nacionalidad != null) {
                nacionalidad = em.getReference(nacionalidad.getClass(), nacionalidad.getId());
                empleadohora.setNacionalidad(nacionalidad);
            }
            Profesion profesionEjercida = empleadohora.getProfesionEjercida();
            if (profesionEjercida != null) {
                profesionEjercida = em.getReference(profesionEjercida.getClass(), profesionEjercida.getId());
                empleadohora.setProfesionEjercida(profesionEjercida);
            }
            Usuario usuario = empleadohora.getUsuario();
            if (usuario != null) {
                usuario = em.getReference(usuario.getClass(), usuario.getId());
                empleadohora.setUsuario(usuario);
            }
            em.persist(empleadohora);
            if (cargoDesempennado != null) {
                cargoDesempennado.getEmpleadohoraList().add(empleadohora);
                cargoDesempennado = em.merge(cargoDesempennado);
            }
            if (municipioActual != null) {
                municipioActual.getEmpleadohoraList().add(empleadohora);
                municipioActual = em.merge(municipioActual);
            }
            if (nacionalidad != null) {
                nacionalidad.getEmpleadohoraList().add(empleadohora);
                nacionalidad = em.merge(nacionalidad);
            }
            if (profesionEjercida != null) {
                profesionEjercida.getEmpleadohoraList().add(empleadohora);
                profesionEjercida = em.merge(profesionEjercida);
            }
            if (usuario != null) {
                usuario.getEmpleadohoraList().add(empleadohora);
                usuario = em.merge(usuario);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findEmpleadohora(empleadohora.getId()) != null) {
                throw new PreexistingEntityException("Empleadohora " + empleadohora + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Empleadohora empleadohora) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Empleadohora persistentEmpleadohora = em.find(Empleadohora.class, empleadohora.getId());
            Cargo cargoDesempennadoOld = persistentEmpleadohora.getCargoDesempennado();
            Cargo cargoDesempennadoNew = empleadohora.getCargoDesempennado();
            Municipio municipioActualOld = persistentEmpleadohora.getMunicipioActual();
            Municipio municipioActualNew = empleadohora.getMunicipioActual();
            Pais nacionalidadOld = persistentEmpleadohora.getNacionalidad();
            Pais nacionalidadNew = empleadohora.getNacionalidad();
            Profesion profesionEjercidaOld = persistentEmpleadohora.getProfesionEjercida();
            Profesion profesionEjercidaNew = empleadohora.getProfesionEjercida();
            Usuario usuarioOld = persistentEmpleadohora.getUsuario();
            Usuario usuarioNew = empleadohora.getUsuario();
            if (cargoDesempennadoNew != null) {
                cargoDesempennadoNew = em.getReference(cargoDesempennadoNew.getClass(), cargoDesempennadoNew.getId());
                empleadohora.setCargoDesempennado(cargoDesempennadoNew);
            }
            if (municipioActualNew != null) {
                municipioActualNew = em.getReference(municipioActualNew.getClass(), municipioActualNew.getId());
                empleadohora.setMunicipioActual(municipioActualNew);
            }
            if (nacionalidadNew != null) {
                nacionalidadNew = em.getReference(nacionalidadNew.getClass(), nacionalidadNew.getId());
                empleadohora.setNacionalidad(nacionalidadNew);
            }
            if (profesionEjercidaNew != null) {
                profesionEjercidaNew = em.getReference(profesionEjercidaNew.getClass(), profesionEjercidaNew.getId());
                empleadohora.setProfesionEjercida(profesionEjercidaNew);
            }
            if (usuarioNew != null) {
                usuarioNew = em.getReference(usuarioNew.getClass(), usuarioNew.getId());
                empleadohora.setUsuario(usuarioNew);
            }
            empleadohora = em.merge(empleadohora);
            if (cargoDesempennadoOld != null && !cargoDesempennadoOld.equals(cargoDesempennadoNew)) {
                cargoDesempennadoOld.getEmpleadohoraList().remove(empleadohora);
                cargoDesempennadoOld = em.merge(cargoDesempennadoOld);
            }
            if (cargoDesempennadoNew != null && !cargoDesempennadoNew.equals(cargoDesempennadoOld)) {
                cargoDesempennadoNew.getEmpleadohoraList().add(empleadohora);
                cargoDesempennadoNew = em.merge(cargoDesempennadoNew);
            }
            if (municipioActualOld != null && !municipioActualOld.equals(municipioActualNew)) {
                municipioActualOld.getEmpleadohoraList().remove(empleadohora);
                municipioActualOld = em.merge(municipioActualOld);
            }
            if (municipioActualNew != null && !municipioActualNew.equals(municipioActualOld)) {
                municipioActualNew.getEmpleadohoraList().add(empleadohora);
                municipioActualNew = em.merge(municipioActualNew);
            }
            if (nacionalidadOld != null && !nacionalidadOld.equals(nacionalidadNew)) {
                nacionalidadOld.getEmpleadohoraList().remove(empleadohora);
                nacionalidadOld = em.merge(nacionalidadOld);
            }
            if (nacionalidadNew != null && !nacionalidadNew.equals(nacionalidadOld)) {
                nacionalidadNew.getEmpleadohoraList().add(empleadohora);
                nacionalidadNew = em.merge(nacionalidadNew);
            }
            if (profesionEjercidaOld != null && !profesionEjercidaOld.equals(profesionEjercidaNew)) {
                profesionEjercidaOld.getEmpleadohoraList().remove(empleadohora);
                profesionEjercidaOld = em.merge(profesionEjercidaOld);
            }
            if (profesionEjercidaNew != null && !profesionEjercidaNew.equals(profesionEjercidaOld)) {
                profesionEjercidaNew.getEmpleadohoraList().add(empleadohora);
                profesionEjercidaNew = em.merge(profesionEjercidaNew);
            }
            if (usuarioOld != null && !usuarioOld.equals(usuarioNew)) {
                usuarioOld.getEmpleadohoraList().remove(empleadohora);
                usuarioOld = em.merge(usuarioOld);
            }
            if (usuarioNew != null && !usuarioNew.equals(usuarioOld)) {
                usuarioNew.getEmpleadohoraList().add(empleadohora);
                usuarioNew = em.merge(usuarioNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = empleadohora.getId();
                if (findEmpleadohora(id) == null) {
                    throw new NonexistentEntityException("The empleadohora with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Empleadohora empleadohora;
            try {
                empleadohora = em.getReference(Empleadohora.class, id);
                empleadohora.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The empleadohora with id " + id + " no longer exists.", enfe);
            }
            Cargo cargoDesempennado = empleadohora.getCargoDesempennado();
            if (cargoDesempennado != null) {
                cargoDesempennado.getEmpleadohoraList().remove(empleadohora);
                cargoDesempennado = em.merge(cargoDesempennado);
            }
            Municipio municipioActual = empleadohora.getMunicipioActual();
            if (municipioActual != null) {
                municipioActual.getEmpleadohoraList().remove(empleadohora);
                municipioActual = em.merge(municipioActual);
            }
            Pais nacionalidad = empleadohora.getNacionalidad();
            if (nacionalidad != null) {
                nacionalidad.getEmpleadohoraList().remove(empleadohora);
                nacionalidad = em.merge(nacionalidad);
            }
            Profesion profesionEjercida = empleadohora.getProfesionEjercida();
            if (profesionEjercida != null) {
                profesionEjercida.getEmpleadohoraList().remove(empleadohora);
                profesionEjercida = em.merge(profesionEjercida);
            }
            Usuario usuario = empleadohora.getUsuario();
            if (usuario != null) {
                usuario.getEmpleadohoraList().remove(empleadohora);
                usuario = em.merge(usuario);
            }
            em.remove(empleadohora);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Empleadohora> findEmpleadohoraEntities() {
        return findEmpleadohoraEntities(true, -1, -1);
    }

    public List<Empleadohora> findEmpleadohoraEntities(int maxResults, int firstResult) {
        return findEmpleadohoraEntities(false, maxResults, firstResult);
    }

    private List<Empleadohora> findEmpleadohoraEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Empleadohora.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Empleadohora findEmpleadohora(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Empleadohora.class, id);
        } finally {
            em.close();
        }
    }

    public int getEmpleadohoraCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Empleadohora> rt = cq.from(Empleadohora.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
