/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import com.controlador.exceptions.NonexistentEntityException;
import com.controlador.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.modelo.Habilidadprofesional;
import com.modelo.Ofertalaboral;
import com.modelo.OfertalaboralHabilidad;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author hola
 */
public class OfertalaboralHabilidadJpaController implements Serializable {

    public OfertalaboralHabilidadJpaController(EntityManagerFactory emf) {
             this.emf = Persistence.createEntityManagerFactory("ProyectoVacantesPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(OfertalaboralHabilidad ofertalaboralHabilidad) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Habilidadprofesional habilidadProfesional = ofertalaboralHabilidad.getHabilidadProfesional();
            if (habilidadProfesional != null) {
                habilidadProfesional = em.getReference(habilidadProfesional.getClass(), habilidadProfesional.getId());
                ofertalaboralHabilidad.setHabilidadProfesional(habilidadProfesional);
            }
            Ofertalaboral ofertaLaboral = ofertalaboralHabilidad.getOfertaLaboral();
            if (ofertaLaboral != null) {
                ofertaLaboral = em.getReference(ofertaLaboral.getClass(), ofertaLaboral.getId());
                ofertalaboralHabilidad.setOfertaLaboral(ofertaLaboral);
            }
            em.persist(ofertalaboralHabilidad);
            if (habilidadProfesional != null) {
                habilidadProfesional.getOfertalaboralHabilidadList().add(ofertalaboralHabilidad);
                habilidadProfesional = em.merge(habilidadProfesional);
            }
            if (ofertaLaboral != null) {
                ofertaLaboral.getOfertalaboralHabilidadList().add(ofertalaboralHabilidad);
                ofertaLaboral = em.merge(ofertaLaboral);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findOfertalaboralHabilidad(ofertalaboralHabilidad.getId()) != null) {
                throw new PreexistingEntityException("OfertalaboralHabilidad " + ofertalaboralHabilidad + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(OfertalaboralHabilidad ofertalaboralHabilidad) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            OfertalaboralHabilidad persistentOfertalaboralHabilidad = em.find(OfertalaboralHabilidad.class, ofertalaboralHabilidad.getId());
            Habilidadprofesional habilidadProfesionalOld = persistentOfertalaboralHabilidad.getHabilidadProfesional();
            Habilidadprofesional habilidadProfesionalNew = ofertalaboralHabilidad.getHabilidadProfesional();
            Ofertalaboral ofertaLaboralOld = persistentOfertalaboralHabilidad.getOfertaLaboral();
            Ofertalaboral ofertaLaboralNew = ofertalaboralHabilidad.getOfertaLaboral();
            if (habilidadProfesionalNew != null) {
                habilidadProfesionalNew = em.getReference(habilidadProfesionalNew.getClass(), habilidadProfesionalNew.getId());
                ofertalaboralHabilidad.setHabilidadProfesional(habilidadProfesionalNew);
            }
            if (ofertaLaboralNew != null) {
                ofertaLaboralNew = em.getReference(ofertaLaboralNew.getClass(), ofertaLaboralNew.getId());
                ofertalaboralHabilidad.setOfertaLaboral(ofertaLaboralNew);
            }
            ofertalaboralHabilidad = em.merge(ofertalaboralHabilidad);
            if (habilidadProfesionalOld != null && !habilidadProfesionalOld.equals(habilidadProfesionalNew)) {
                habilidadProfesionalOld.getOfertalaboralHabilidadList().remove(ofertalaboralHabilidad);
                habilidadProfesionalOld = em.merge(habilidadProfesionalOld);
            }
            if (habilidadProfesionalNew != null && !habilidadProfesionalNew.equals(habilidadProfesionalOld)) {
                habilidadProfesionalNew.getOfertalaboralHabilidadList().add(ofertalaboralHabilidad);
                habilidadProfesionalNew = em.merge(habilidadProfesionalNew);
            }
            if (ofertaLaboralOld != null && !ofertaLaboralOld.equals(ofertaLaboralNew)) {
                ofertaLaboralOld.getOfertalaboralHabilidadList().remove(ofertalaboralHabilidad);
                ofertaLaboralOld = em.merge(ofertaLaboralOld);
            }
            if (ofertaLaboralNew != null && !ofertaLaboralNew.equals(ofertaLaboralOld)) {
                ofertaLaboralNew.getOfertalaboralHabilidadList().add(ofertalaboralHabilidad);
                ofertaLaboralNew = em.merge(ofertaLaboralNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ofertalaboralHabilidad.getId();
                if (findOfertalaboralHabilidad(id) == null) {
                    throw new NonexistentEntityException("The ofertalaboralHabilidad with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            OfertalaboralHabilidad ofertalaboralHabilidad;
            try {
                ofertalaboralHabilidad = em.getReference(OfertalaboralHabilidad.class, id);
                ofertalaboralHabilidad.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ofertalaboralHabilidad with id " + id + " no longer exists.", enfe);
            }
            Habilidadprofesional habilidadProfesional = ofertalaboralHabilidad.getHabilidadProfesional();
            if (habilidadProfesional != null) {
                habilidadProfesional.getOfertalaboralHabilidadList().remove(ofertalaboralHabilidad);
                habilidadProfesional = em.merge(habilidadProfesional);
            }
            Ofertalaboral ofertaLaboral = ofertalaboralHabilidad.getOfertaLaboral();
            if (ofertaLaboral != null) {
                ofertaLaboral.getOfertalaboralHabilidadList().remove(ofertalaboralHabilidad);
                ofertaLaboral = em.merge(ofertaLaboral);
            }
            em.remove(ofertalaboralHabilidad);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<OfertalaboralHabilidad> findOfertalaboralHabilidadEntities() {
        return findOfertalaboralHabilidadEntities(true, -1, -1);
    }

    public List<OfertalaboralHabilidad> findOfertalaboralHabilidadEntities(int maxResults, int firstResult) {
        return findOfertalaboralHabilidadEntities(false, maxResults, firstResult);
    }

    private List<OfertalaboralHabilidad> findOfertalaboralHabilidadEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(OfertalaboralHabilidad.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public OfertalaboralHabilidad findOfertalaboralHabilidad(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(OfertalaboralHabilidad.class, id);
        } finally {
            em.close();
        }
    }

    public int getOfertalaboralHabilidadCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<OfertalaboralHabilidad> rt = cq.from(OfertalaboralHabilidad.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
