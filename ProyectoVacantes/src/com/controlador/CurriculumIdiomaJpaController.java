/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import com.controlador.exceptions.NonexistentEntityException;
import com.controlador.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.modelo.Curriculum;
import com.modelo.CurriculumIdioma;
import com.modelo.Idioma;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author hola
 */
public class CurriculumIdiomaJpaController implements Serializable {

    public CurriculumIdiomaJpaController(EntityManagerFactory emf) {
           this.emf = Persistence.createEntityManagerFactory("ProyectoVacantesPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CurriculumIdioma curriculumIdioma) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Curriculum curriculum = curriculumIdioma.getCurriculum();
            if (curriculum != null) {
                curriculum = em.getReference(curriculum.getClass(), curriculum.getId());
                curriculumIdioma.setCurriculum(curriculum);
            }
            Idioma idioma = curriculumIdioma.getIdioma();
            if (idioma != null) {
                idioma = em.getReference(idioma.getClass(), idioma.getId());
                curriculumIdioma.setIdioma(idioma);
            }
            em.persist(curriculumIdioma);
            if (curriculum != null) {
                curriculum.getCurriculumIdiomaList().add(curriculumIdioma);
                curriculum = em.merge(curriculum);
            }
            if (idioma != null) {
                idioma.getCurriculumIdiomaList().add(curriculumIdioma);
                idioma = em.merge(idioma);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCurriculumIdioma(curriculumIdioma.getId()) != null) {
                throw new PreexistingEntityException("CurriculumIdioma " + curriculumIdioma + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CurriculumIdioma curriculumIdioma) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CurriculumIdioma persistentCurriculumIdioma = em.find(CurriculumIdioma.class, curriculumIdioma.getId());
            Curriculum curriculumOld = persistentCurriculumIdioma.getCurriculum();
            Curriculum curriculumNew = curriculumIdioma.getCurriculum();
            Idioma idiomaOld = persistentCurriculumIdioma.getIdioma();
            Idioma idiomaNew = curriculumIdioma.getIdioma();
            if (curriculumNew != null) {
                curriculumNew = em.getReference(curriculumNew.getClass(), curriculumNew.getId());
                curriculumIdioma.setCurriculum(curriculumNew);
            }
            if (idiomaNew != null) {
                idiomaNew = em.getReference(idiomaNew.getClass(), idiomaNew.getId());
                curriculumIdioma.setIdioma(idiomaNew);
            }
            curriculumIdioma = em.merge(curriculumIdioma);
            if (curriculumOld != null && !curriculumOld.equals(curriculumNew)) {
                curriculumOld.getCurriculumIdiomaList().remove(curriculumIdioma);
                curriculumOld = em.merge(curriculumOld);
            }
            if (curriculumNew != null && !curriculumNew.equals(curriculumOld)) {
                curriculumNew.getCurriculumIdiomaList().add(curriculumIdioma);
                curriculumNew = em.merge(curriculumNew);
            }
            if (idiomaOld != null && !idiomaOld.equals(idiomaNew)) {
                idiomaOld.getCurriculumIdiomaList().remove(curriculumIdioma);
                idiomaOld = em.merge(idiomaOld);
            }
            if (idiomaNew != null && !idiomaNew.equals(idiomaOld)) {
                idiomaNew.getCurriculumIdiomaList().add(curriculumIdioma);
                idiomaNew = em.merge(idiomaNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = curriculumIdioma.getId();
                if (findCurriculumIdioma(id) == null) {
                    throw new NonexistentEntityException("The curriculumIdioma with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CurriculumIdioma curriculumIdioma;
            try {
                curriculumIdioma = em.getReference(CurriculumIdioma.class, id);
                curriculumIdioma.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The curriculumIdioma with id " + id + " no longer exists.", enfe);
            }
            Curriculum curriculum = curriculumIdioma.getCurriculum();
            if (curriculum != null) {
                curriculum.getCurriculumIdiomaList().remove(curriculumIdioma);
                curriculum = em.merge(curriculum);
            }
            Idioma idioma = curriculumIdioma.getIdioma();
            if (idioma != null) {
                idioma.getCurriculumIdiomaList().remove(curriculumIdioma);
                idioma = em.merge(idioma);
            }
            em.remove(curriculumIdioma);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CurriculumIdioma> findCurriculumIdiomaEntities() {
        return findCurriculumIdiomaEntities(true, -1, -1);
    }

    public List<CurriculumIdioma> findCurriculumIdiomaEntities(int maxResults, int firstResult) {
        return findCurriculumIdiomaEntities(false, maxResults, firstResult);
    }

    private List<CurriculumIdioma> findCurriculumIdiomaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CurriculumIdioma.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CurriculumIdioma findCurriculumIdioma(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CurriculumIdioma.class, id);
        } finally {
            em.close();
        }
    }

    public int getCurriculumIdiomaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CurriculumIdioma> rt = cq.from(CurriculumIdioma.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
