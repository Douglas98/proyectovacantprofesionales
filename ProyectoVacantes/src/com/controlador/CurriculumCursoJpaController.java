/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import com.controlador.exceptions.NonexistentEntityException;
import com.controlador.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.modelo.Curriculum;
import com.modelo.CurriculumCurso;
import com.modelo.Curso;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author hola
 */
public class CurriculumCursoJpaController implements Serializable {

    public CurriculumCursoJpaController(EntityManagerFactory emf) {
             this.emf = Persistence.createEntityManagerFactory("ProyectoVacantesPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CurriculumCurso curriculumCurso) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Curriculum curriculum = curriculumCurso.getCurriculum();
            if (curriculum != null) {
                curriculum = em.getReference(curriculum.getClass(), curriculum.getId());
                curriculumCurso.setCurriculum(curriculum);
            }
            Curso curso = curriculumCurso.getCurso();
            if (curso != null) {
                curso = em.getReference(curso.getClass(), curso.getId());
                curriculumCurso.setCurso(curso);
            }
            em.persist(curriculumCurso);
            if (curriculum != null) {
                curriculum.getCurriculumCursoList().add(curriculumCurso);
                curriculum = em.merge(curriculum);
            }
            if (curso != null) {
                curso.getCurriculumCursoList().add(curriculumCurso);
                curso = em.merge(curso);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCurriculumCurso(curriculumCurso.getId()) != null) {
                throw new PreexistingEntityException("CurriculumCurso " + curriculumCurso + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CurriculumCurso curriculumCurso) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CurriculumCurso persistentCurriculumCurso = em.find(CurriculumCurso.class, curriculumCurso.getId());
            Curriculum curriculumOld = persistentCurriculumCurso.getCurriculum();
            Curriculum curriculumNew = curriculumCurso.getCurriculum();
            Curso cursoOld = persistentCurriculumCurso.getCurso();
            Curso cursoNew = curriculumCurso.getCurso();
            if (curriculumNew != null) {
                curriculumNew = em.getReference(curriculumNew.getClass(), curriculumNew.getId());
                curriculumCurso.setCurriculum(curriculumNew);
            }
            if (cursoNew != null) {
                cursoNew = em.getReference(cursoNew.getClass(), cursoNew.getId());
                curriculumCurso.setCurso(cursoNew);
            }
            curriculumCurso = em.merge(curriculumCurso);
            if (curriculumOld != null && !curriculumOld.equals(curriculumNew)) {
                curriculumOld.getCurriculumCursoList().remove(curriculumCurso);
                curriculumOld = em.merge(curriculumOld);
            }
            if (curriculumNew != null && !curriculumNew.equals(curriculumOld)) {
                curriculumNew.getCurriculumCursoList().add(curriculumCurso);
                curriculumNew = em.merge(curriculumNew);
            }
            if (cursoOld != null && !cursoOld.equals(cursoNew)) {
                cursoOld.getCurriculumCursoList().remove(curriculumCurso);
                cursoOld = em.merge(cursoOld);
            }
            if (cursoNew != null && !cursoNew.equals(cursoOld)) {
                cursoNew.getCurriculumCursoList().add(curriculumCurso);
                cursoNew = em.merge(cursoNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = curriculumCurso.getId();
                if (findCurriculumCurso(id) == null) {
                    throw new NonexistentEntityException("The curriculumCurso with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CurriculumCurso curriculumCurso;
            try {
                curriculumCurso = em.getReference(CurriculumCurso.class, id);
                curriculumCurso.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The curriculumCurso with id " + id + " no longer exists.", enfe);
            }
            Curriculum curriculum = curriculumCurso.getCurriculum();
            if (curriculum != null) {
                curriculum.getCurriculumCursoList().remove(curriculumCurso);
                curriculum = em.merge(curriculum);
            }
            Curso curso = curriculumCurso.getCurso();
            if (curso != null) {
                curso.getCurriculumCursoList().remove(curriculumCurso);
                curso = em.merge(curso);
            }
            em.remove(curriculumCurso);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CurriculumCurso> findCurriculumCursoEntities() {
        return findCurriculumCursoEntities(true, -1, -1);
    }

    public List<CurriculumCurso> findCurriculumCursoEntities(int maxResults, int firstResult) {
        return findCurriculumCursoEntities(false, maxResults, firstResult);
    }

    private List<CurriculumCurso> findCurriculumCursoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CurriculumCurso.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CurriculumCurso findCurriculumCurso(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CurriculumCurso.class, id);
        } finally {
            em.close();
        }
    }

    public int getCurriculumCursoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CurriculumCurso> rt = cq.from(CurriculumCurso.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
