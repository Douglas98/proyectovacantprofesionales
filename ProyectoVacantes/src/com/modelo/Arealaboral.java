/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author hola
 */
@Entity
@Table(name = "arealaboral")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Arealaboral.findAll", query = "SELECT a FROM Arealaboral a")
    , @NamedQuery(name = "Arealaboral.findById", query = "SELECT a FROM Arealaboral a WHERE a.id = :id")
    , @NamedQuery(name = "Arealaboral.findByNombre", query = "SELECT a FROM Arealaboral a WHERE a.nombre = :nombre")
    , @NamedQuery(name = "Arealaboral.findByBorrado", query = "SELECT a FROM Arealaboral a WHERE a.borrado = :borrado")})
public class Arealaboral implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "borrado")
    private boolean borrado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "areaLaboral")
    private List<Habilidadprofesional> habilidadprofesionalList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "areaLaboral")
    private List<Profesion> profesionList;

    public Arealaboral() {
    }

    public Arealaboral(Integer id) {
        this.id = id;
    }

    public Arealaboral(Integer id, String nombre, boolean borrado) {
        this.id = id;
        this.nombre = nombre;
        this.borrado = borrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(boolean borrado) {
        this.borrado = borrado;
    }

    @XmlTransient
    public List<Habilidadprofesional> getHabilidadprofesionalList() {
        return habilidadprofesionalList;
    }

    public void setHabilidadprofesionalList(List<Habilidadprofesional> habilidadprofesionalList) {
        this.habilidadprofesionalList = habilidadprofesionalList;
    }

    @XmlTransient
    public List<Profesion> getProfesionList() {
        return profesionList;
    }

    public void setProfesionList(List<Profesion> profesionList) {
        this.profesionList = profesionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Arealaboral)) {
            return false;
        }
        Arealaboral other = (Arealaboral) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.modelo.Arealaboral[ id=" + id + " ]";
    }
    
}
