/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hola
 */
@Entity
@Table(name = "curriculum_profesion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CurriculumProfesion.findAll", query = "SELECT c FROM CurriculumProfesion c")
    , @NamedQuery(name = "CurriculumProfesion.findById", query = "SELECT c FROM CurriculumProfesion c WHERE c.id = :id")
    , @NamedQuery(name = "CurriculumProfesion.findByCorrelativo", query = "SELECT c FROM CurriculumProfesion c WHERE c.correlativo = :correlativo")
    , @NamedQuery(name = "CurriculumProfesion.findByAnnosExperiencia", query = "SELECT c FROM CurriculumProfesion c WHERE c.annosExperiencia = :annosExperiencia")
    , @NamedQuery(name = "CurriculumProfesion.findByBorrado", query = "SELECT c FROM CurriculumProfesion c WHERE c.borrado = :borrado")})
public class CurriculumProfesion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "correlativo")
    private int correlativo;
    @Basic(optional = false)
    @Column(name = "annosExperiencia")
    private int annosExperiencia;
    @Basic(optional = false)
    @Column(name = "borrado")
    private boolean borrado;
    @JoinColumn(name = "curriculum", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Curriculum curriculum;
    @JoinColumn(name = "profesion", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Profesion profesion;

    public CurriculumProfesion() {
    }

    public CurriculumProfesion(Integer id) {
        this.id = id;
    }

    public CurriculumProfesion(Integer id, int correlativo, int annosExperiencia, boolean borrado) {
        this.id = id;
        this.correlativo = correlativo;
        this.annosExperiencia = annosExperiencia;
        this.borrado = borrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(int correlativo) {
        this.correlativo = correlativo;
    }

    public int getAnnosExperiencia() {
        return annosExperiencia;
    }

    public void setAnnosExperiencia(int annosExperiencia) {
        this.annosExperiencia = annosExperiencia;
    }

    public boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(boolean borrado) {
        this.borrado = borrado;
    }

    public Curriculum getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(Curriculum curriculum) {
        this.curriculum = curriculum;
    }

    public Profesion getProfesion() {
        return profesion;
    }

    public void setProfesion(Profesion profesion) {
        this.profesion = profesion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CurriculumProfesion)) {
            return false;
        }
        CurriculumProfesion other = (CurriculumProfesion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.modelo.CurriculumProfesion[ id=" + id + " ]";
    }
    
}
