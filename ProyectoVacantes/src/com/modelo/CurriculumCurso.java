/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hola
 */
@Entity
@Table(name = "curriculum_curso")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CurriculumCurso.findAll", query = "SELECT c FROM CurriculumCurso c")
    , @NamedQuery(name = "CurriculumCurso.findById", query = "SELECT c FROM CurriculumCurso c WHERE c.id = :id")
    , @NamedQuery(name = "CurriculumCurso.findByCorrelativo", query = "SELECT c FROM CurriculumCurso c WHERE c.correlativo = :correlativo")
    , @NamedQuery(name = "CurriculumCurso.findByBorrado", query = "SELECT c FROM CurriculumCurso c WHERE c.borrado = :borrado")})
public class CurriculumCurso implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "correlativo")
    private int correlativo;
    @Basic(optional = false)
    @Column(name = "borrado")
    private boolean borrado;
    @JoinColumn(name = "curriculum", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Curriculum curriculum;
    @JoinColumn(name = "curso", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Curso curso;

    public CurriculumCurso() {
    }

    public CurriculumCurso(Integer id) {
        this.id = id;
    }

    public CurriculumCurso(Integer id, int correlativo, boolean borrado) {
        this.id = id;
        this.correlativo = correlativo;
        this.borrado = borrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(int correlativo) {
        this.correlativo = correlativo;
    }

    public boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(boolean borrado) {
        this.borrado = borrado;
    }

    public Curriculum getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(Curriculum curriculum) {
        this.curriculum = curriculum;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CurriculumCurso)) {
            return false;
        }
        CurriculumCurso other = (CurriculumCurso) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.modelo.CurriculumCurso[ id=" + id + " ]";
    }
    
}
