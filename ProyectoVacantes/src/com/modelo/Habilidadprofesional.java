/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author hola
 */
@Entity
@Table(name = "habilidadprofesional")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Habilidadprofesional.findAll", query = "SELECT h FROM Habilidadprofesional h")
    , @NamedQuery(name = "Habilidadprofesional.findById", query = "SELECT h FROM Habilidadprofesional h WHERE h.id = :id")
    , @NamedQuery(name = "Habilidadprofesional.findByNombre", query = "SELECT h FROM Habilidadprofesional h WHERE h.nombre = :nombre")
    , @NamedQuery(name = "Habilidadprofesional.findByBorrado", query = "SELECT h FROM Habilidadprofesional h WHERE h.borrado = :borrado")})
public class Habilidadprofesional implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "borrado")
    private boolean borrado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "habilidadProfesional")
    private List<OfertalaboralHabilidad> ofertalaboralHabilidadList;
    @JoinColumn(name = "areaLaboral", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Arealaboral areaLaboral;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "habilidadProfesional")
    private List<CurriculumHabilidad> curriculumHabilidadList;

    public Habilidadprofesional() {
    }

    public Habilidadprofesional(Integer id) {
        this.id = id;
    }

    public Habilidadprofesional(Integer id, String nombre, boolean borrado) {
        this.id = id;
        this.nombre = nombre;
        this.borrado = borrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(boolean borrado) {
        this.borrado = borrado;
    }

    @XmlTransient
    public List<OfertalaboralHabilidad> getOfertalaboralHabilidadList() {
        return ofertalaboralHabilidadList;
    }

    public void setOfertalaboralHabilidadList(List<OfertalaboralHabilidad> ofertalaboralHabilidadList) {
        this.ofertalaboralHabilidadList = ofertalaboralHabilidadList;
    }

    public Arealaboral getAreaLaboral() {
        return areaLaboral;
    }

    public void setAreaLaboral(Arealaboral areaLaboral) {
        this.areaLaboral = areaLaboral;
    }

    @XmlTransient
    public List<CurriculumHabilidad> getCurriculumHabilidadList() {
        return curriculumHabilidadList;
    }

    public void setCurriculumHabilidadList(List<CurriculumHabilidad> curriculumHabilidadList) {
        this.curriculumHabilidadList = curriculumHabilidadList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Habilidadprofesional)) {
            return false;
        }
        Habilidadprofesional other = (Habilidadprofesional) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.modelo.Habilidadprofesional[ id=" + id + " ]";
    }
    
}
