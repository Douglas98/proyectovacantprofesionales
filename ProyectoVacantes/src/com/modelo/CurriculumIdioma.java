/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hola
 */
@Entity
@Table(name = "curriculum_idioma")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CurriculumIdioma.findAll", query = "SELECT c FROM CurriculumIdioma c")
    , @NamedQuery(name = "CurriculumIdioma.findById", query = "SELECT c FROM CurriculumIdioma c WHERE c.id = :id")
    , @NamedQuery(name = "CurriculumIdioma.findByCorrelativo", query = "SELECT c FROM CurriculumIdioma c WHERE c.correlativo = :correlativo")
    , @NamedQuery(name = "CurriculumIdioma.findByNivelDominio", query = "SELECT c FROM CurriculumIdioma c WHERE c.nivelDominio = :nivelDominio")
    , @NamedQuery(name = "CurriculumIdioma.findByBorrado", query = "SELECT c FROM CurriculumIdioma c WHERE c.borrado = :borrado")})
public class CurriculumIdioma implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "correlativo")
    private int correlativo;
    @Basic(optional = false)
    @Column(name = "nivelDominio")
    private String nivelDominio;
    @Basic(optional = false)
    @Column(name = "borrado")
    private boolean borrado;
    @JoinColumn(name = "curriculum", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Curriculum curriculum;
    @JoinColumn(name = "idioma", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Idioma idioma;

    public CurriculumIdioma() {
    }

    public CurriculumIdioma(Integer id) {
        this.id = id;
    }

    public CurriculumIdioma(Integer id, int correlativo, String nivelDominio, boolean borrado) {
        this.id = id;
        this.correlativo = correlativo;
        this.nivelDominio = nivelDominio;
        this.borrado = borrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(int correlativo) {
        this.correlativo = correlativo;
    }

    public String getNivelDominio() {
        return nivelDominio;
    }

    public void setNivelDominio(String nivelDominio) {
        this.nivelDominio = nivelDominio;
    }

    public boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(boolean borrado) {
        this.borrado = borrado;
    }

    public Curriculum getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(Curriculum curriculum) {
        this.curriculum = curriculum;
    }

    public Idioma getIdioma() {
        return idioma;
    }

    public void setIdioma(Idioma idioma) {
        this.idioma = idioma;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CurriculumIdioma)) {
            return false;
        }
        CurriculumIdioma other = (CurriculumIdioma) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.modelo.CurriculumIdioma[ id=" + id + " ]";
    }
    
}
