/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hola
 */
@Entity
@Table(name = "ofertalaboral_habilidad")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OfertalaboralHabilidad.findAll", query = "SELECT o FROM OfertalaboralHabilidad o")
    , @NamedQuery(name = "OfertalaboralHabilidad.findById", query = "SELECT o FROM OfertalaboralHabilidad o WHERE o.id = :id")
    , @NamedQuery(name = "OfertalaboralHabilidad.findByCorrelativo", query = "SELECT o FROM OfertalaboralHabilidad o WHERE o.correlativo = :correlativo")
    , @NamedQuery(name = "OfertalaboralHabilidad.findByNivelDominio", query = "SELECT o FROM OfertalaboralHabilidad o WHERE o.nivelDominio = :nivelDominio")
    , @NamedQuery(name = "OfertalaboralHabilidad.findByBorrado", query = "SELECT o FROM OfertalaboralHabilidad o WHERE o.borrado = :borrado")})
public class OfertalaboralHabilidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "correlativo")
    private int correlativo;
    @Basic(optional = false)
    @Column(name = "nivelDominio")
    private String nivelDominio;
    @Basic(optional = false)
    @Column(name = "borrado")
    private boolean borrado;
    @JoinColumn(name = "habilidadProfesional", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Habilidadprofesional habilidadProfesional;
    @JoinColumn(name = "ofertaLaboral", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Ofertalaboral ofertaLaboral;

    public OfertalaboralHabilidad() {
    }

    public OfertalaboralHabilidad(Integer id) {
        this.id = id;
    }

    public OfertalaboralHabilidad(Integer id, int correlativo, String nivelDominio, boolean borrado) {
        this.id = id;
        this.correlativo = correlativo;
        this.nivelDominio = nivelDominio;
        this.borrado = borrado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(int correlativo) {
        this.correlativo = correlativo;
    }

    public String getNivelDominio() {
        return nivelDominio;
    }

    public void setNivelDominio(String nivelDominio) {
        this.nivelDominio = nivelDominio;
    }

    public boolean getBorrado() {
        return borrado;
    }

    public void setBorrado(boolean borrado) {
        this.borrado = borrado;
    }

    public Habilidadprofesional getHabilidadProfesional() {
        return habilidadProfesional;
    }

    public void setHabilidadProfesional(Habilidadprofesional habilidadProfesional) {
        this.habilidadProfesional = habilidadProfesional;
    }

    public Ofertalaboral getOfertaLaboral() {
        return ofertaLaboral;
    }

    public void setOfertaLaboral(Ofertalaboral ofertaLaboral) {
        this.ofertaLaboral = ofertaLaboral;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OfertalaboralHabilidad)) {
            return false;
        }
        OfertalaboralHabilidad other = (OfertalaboralHabilidad) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.modelo.OfertalaboralHabilidad[ id=" + id + " ]";
    }
    
}
