/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hola
 */
@Entity
@Table(name = "empleadoasalariado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empleadoasalariado.findAll", query = "SELECT e FROM Empleadoasalariado e")
    , @NamedQuery(name = "Empleadoasalariado.findById", query = "SELECT e FROM Empleadoasalariado e WHERE e.id = :id")
    , @NamedQuery(name = "Empleadoasalariado.findByDui", query = "SELECT e FROM Empleadoasalariado e WHERE e.dui = :dui")
    , @NamedQuery(name = "Empleadoasalariado.findByNit", query = "SELECT e FROM Empleadoasalariado e WHERE e.nit = :nit")
    , @NamedQuery(name = "Empleadoasalariado.findByNombre", query = "SELECT e FROM Empleadoasalariado e WHERE e.nombre = :nombre")
    , @NamedQuery(name = "Empleadoasalariado.findByApellido", query = "SELECT e FROM Empleadoasalariado e WHERE e.apellido = :apellido")
    , @NamedQuery(name = "Empleadoasalariado.findByGenero", query = "SELECT e FROM Empleadoasalariado e WHERE e.genero = :genero")
    , @NamedQuery(name = "Empleadoasalariado.findByFechaNacimiento", query = "SELECT e FROM Empleadoasalariado e WHERE e.fechaNacimiento = :fechaNacimiento")
    , @NamedQuery(name = "Empleadoasalariado.findByDireccion", query = "SELECT e FROM Empleadoasalariado e WHERE e.direccion = :direccion")
    , @NamedQuery(name = "Empleadoasalariado.findBySalario", query = "SELECT e FROM Empleadoasalariado e WHERE e.salario = :salario")})
public class Empleadoasalariado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "dui")
    private String dui;
    @Basic(optional = false)
    @Column(name = "nit")
    private String nit;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "apellido")
    private String apellido;
    @Basic(optional = false)
    @Column(name = "genero")
    private String genero;
    @Basic(optional = false)
    @Column(name = "fechaNacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaNacimiento;
    @Basic(optional = false)
    @Column(name = "direccion")
    private String direccion;
    @Lob
    @Column(name = "foto")
    private byte[] foto;
    @Basic(optional = false)
    @Column(name = "salario")
    private long salario;
    @JoinColumn(name = "cargoDesempennado", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Cargo cargoDesempennado;
    @JoinColumn(name = "municipioActual", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Municipio municipioActual;
    @JoinColumn(name = "nacionalidad", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Pais nacionalidad;
    @JoinColumn(name = "profesionEjercida", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Profesion profesionEjercida;
    @JoinColumn(name = "usuario", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Usuario usuario;

    public Empleadoasalariado() {
    }

    public Empleadoasalariado(Integer id) {
        this.id = id;
    }

    public Empleadoasalariado(Integer id, String dui, String nit, String nombre, String apellido, String genero, Date fechaNacimiento, String direccion, long salario) {
        this.id = id;
        this.dui = dui;
        this.nit = nit;
        this.nombre = nombre;
        this.apellido = apellido;
        this.genero = genero;
        this.fechaNacimiento = fechaNacimiento;
        this.direccion = direccion;
        this.salario = salario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDui() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui = dui;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public long getSalario() {
        return salario;
    }

    public void setSalario(long salario) {
        this.salario = salario;
    }

    public Cargo getCargoDesempennado() {
        return cargoDesempennado;
    }

    public void setCargoDesempennado(Cargo cargoDesempennado) {
        this.cargoDesempennado = cargoDesempennado;
    }

    public Municipio getMunicipioActual() {
        return municipioActual;
    }

    public void setMunicipioActual(Municipio municipioActual) {
        this.municipioActual = municipioActual;
    }

    public Pais getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(Pais nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public Profesion getProfesionEjercida() {
        return profesionEjercida;
    }

    public void setProfesionEjercida(Profesion profesionEjercida) {
        this.profesionEjercida = profesionEjercida;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empleadoasalariado)) {
            return false;
        }
        Empleadoasalariado other = (Empleadoasalariado) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.modelo.Empleadoasalariado[ id=" + id + " ]";
    }
    
}
